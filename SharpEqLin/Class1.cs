﻿using System;
using System.Collections.Generic;

namespace SharpEqLin
{
	public class EquationParseException:Exception
	{

	}
	public class EquationSystemParseException:Exception
	{

	}

	public class LinearEquation
	{
		bool objectDisposed;

		List<char> variables;
		List<int> constants;

		string equation;

		public List<char> Variables { get { return variables; } }
		public List<int> Constants { get { return constants; } }

		private void ParseVariables()
		{
			foreach (char ch in equation)
				if (IsVariable(ch) && !variables.Contains(ch))
					variables.Add(ch);
		}
		private void ParseConstants()
		{
			foreach (char ch in equation)
				if (IsConstant(ch) && !constants.Contains(ch))
					constants.Add(ch);
		}
		private void Parse()
		{
			ParseVariables();
			ParseConstants();
		}

		private bool IsConstant(char ch)
		{
			int p;
			return int.TryParse(ch.ToString(), out p);
		}
		private bool IsVariable(char ch)
		{
			return ch >= 'a' && ch <= 'z' || ch >= 'A' && ch <= 'Z';
		}

		/// <summary>
		/// The default constructor for a LinearEquation object
		/// </summary>
		public LinearEquation()
		{
			equation = string.Empty;
			variables = null;
			constants = null;
		}
		/// <summary>
		/// Given a linear equation, parses it to the variables and constants, or throws an EquationParseException
		/// </summary>
		/// <param name="linearEquation"></param>
		/// <exception cref="EquationParseException"/>
		public LinearEquation(string linearEquation)
		{
			equation = linearEquation;
			Parse();
		}
	}
	public class LinearEquationSystem
	{
		List<LinearEquation> system;
		List<char> uniqueVariables;

		int totalVariables;

		/// <summary>
		/// The default constructor for a LinearEquationSystem object
		/// </summary>
		public LinearEquationSystem()
		{
			system = null;
			uniqueVariables = null;
			totalVariables = 0;
		}
		/// <summary>
		/// Given a list of Linear equations, constructs a system of linear equations, or throws an EquationSystemParseException
		/// </summary>
		/// <param name="equationSystem"></param>
		/// <exception cref="EquationSystemParseException"/>
		public LinearEquationSystem(List<LinearEquation> equationSystem)
		{
			system = equationSystem;
			totalVariables = new Func<int>(() => 
			{
				foreach (LinearEquation eqn in equationSystem)
					foreach (char variable in eqn.Variables)
						if (!uniqueVariables.Contains(variable))
							uniqueVariables.Add(variable);
				return uniqueVariables.Count;
			}).Invoke();
		}
		/// <summary>
		/// Evaluates if the given system of equations is solvable
		/// </summary>
		public bool IsSolvable
		{
			get
			{
				//The number of variables involved must be at least equal to the total number of equations involved.
				return totalVariables <= system.Count;
			}
		}
	}
}
