#include "..\EqLinParser\parser.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
	if (argc < 2 || argc>=3) {
		printf("Usage: parse-test <linear equation>\n");
		return -1;
	}
	printf("Inside parse-test main\n");
	char *eqn = *argv[1];//, *leftSide = LeftHandSide(eqn), *rightSide = RightHandSide(eqn);
	printf("The equation given is %s\nLeft Hand side: %s\nRight hand side :%s\n",eqn);
	return 0;
}