#pragma once

/*
	Define an API to parse a linear equation
*/

char* LeftHandSide(char* linearEquation);
char* RightHandSide(char* linearEquation);
int StringLength(char* stringInput);
int GetIndex(char ch, char* stringInput);