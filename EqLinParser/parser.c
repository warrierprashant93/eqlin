#include <stdio.h>
int StringLength(char* stringInput) {
	int len = 0;
	printf("Inside StringLength");
	while(stringInput[len++]!='\0'){}
	return len;
}

int GetIndex(char ch, char* stringInput) {
	int i = 0;
	printf("Inside GetIndex\n");
	while(ch!=stringInput[i++]){}
	return i;
}

char* LeftHandSide(char* linearEquation) {
	char* lhs = "";
	printf("Inside LeftHandSide\n");
	int i = 0;
	while (linearEquation[i++] != '=')
		*(lhs + i) = linearEquation[i];
	return lhs;
}

char* RightHandSide(char* linearEquation) {
	char* rhs = "";
	printf("Inside RightHandSide\n");
	int i = GetIndex('=', linearEquation), j = StringLength(linearEquation), k = 0;
	while ((i < j) && (k < j - i)) {
		*(rhs + k) = linearEquation[i];
		i++, k++;
	}
	return rhs;
}